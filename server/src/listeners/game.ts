import {EnhancedWebSocket, GameState, ServerSocket} from "../socket";
import {WebSocketMessageData, WebSocketRoomIndexMessageData, WebSocketRoomMessageData} from "./connection";
import {TexasHoldEmPokerGameType, Player, ICard, Deck} from "typedeck";


export class GameListeners {

  Socket: EnhancedWebSocket;
  Server: ServerSocket;

  constructor(socket: EnhancedWebSocket, serverSocket: ServerSocket) {
    this.Socket = socket;
    this.Server = serverSocket;
  }

  StartListeners = () => {
    const socket = this.Socket;
    const server = this.Server;
    const gameListeners = this;

    socket.on('message', function message(message: WebSocketMessageData, isBinary: boolean) {
      if (isBinary) {
        return;
      }

      const rawData = message.toString();
      const data = JSON.parse(rawData);
      let roomData;

      switch (data.action) {
        case 'start-game':
          roomData = data as WebSocketRoomMessageData;
          gameListeners.StartGame(roomData.room);
          break;

        case 'draw-card-deck':
          roomData = data as WebSocketRoomMessageData;
          gameListeners.DrawCard(this.id, roomData.room, true);
          break;

        case 'draw-card-discard':
          roomData = data as WebSocketRoomMessageData;
          gameListeners.DrawCard(this.id, roomData.room, false);
          break;

        case 'place-hand-card':
          roomData = data as WebSocketRoomIndexMessageData;
          gameListeners.PlaceHandCard(this.id, roomData.room, roomData.index);
          break;

        case 'discard-hand-card':
          roomData = data as WebSocketRoomMessageData;
          gameListeners.DiscardHandCard(this.id, roomData.room, false);
          break;


        default:
          break;
      }
    });


  }

  StartGame(room : string) {
    console.log(`room ${room} was started`);

    const roomUsers = Object.values(this.Server.roomUsers[room]);

    if (!Object.keys(this.Server.games).includes(room)) {
      this.CreateGame(room, roomUsers);
    }

    const game = this.Server.games[room];

    this.Server.SaveGames();
    this.Server.SendRoomMessage('game_started', room, {room, game});
  }

  PlaceHandCard(user: string, room : string, handIndex : number) {
    let game = this.Server.games[room];

    if (game.holding == null) {
      // @todo error handling.
      return;
    }

    // @todo I need to know which card was replaced with the holding card.
    console.log(game.hands[user]);
    const handCard = game.hands[user][handIndex];
    game.hands[user][handIndex] = game.holding;
    game.discard?.addCard(handCard);

    game.holding = null;


    this.Server.SaveGames()
    this.Server.SendRoomMessage('card_placed', room, {user, room, game});
  }

  DiscardHandCard(user: string, room : string, deck : boolean) {
    let game = this.Server.games[room];

    if (game.holding == null) {
      // @todo error handling.
      return;
    }

    game.discard?.addCard(game.holding);
    game.holding = null;

    this.Server.SaveGames();
    this.Server.SendRoomMessage('card_placed', room, {user, room, game});
  }

  DrawCard(user: string, room : string, deck : boolean) {
    // @todo fail if room doesn't exist.
    let game = this.Server.games[room];

    // @todo make sure there are cards to draw.
    if (deck) {
      game.holding = game.deck?.takeCard() ?? null;
    }
    else {
      game.holding = game.discard?.takeCard() ?? null;
    }

    this.Server.SaveGames();
    this.Server.SendRoomMessage('card_drawn', room, {user, room, game});
  }

  CreateGame(room : string, roomUsers: string[]) {
    const game = {
      id: room,
      users: roomUsers,
      holding: null,
      currentUser: roomUsers[0],
      deck: null,
      discard: null,
      hands: {},
      turn: 0,
    } as GameState;

    const deck = new TexasHoldEmPokerGameType().createDeck();
    deck.shuffle();
    let players : Player[] = [];
    roomUsers.forEach((user) => {
      players.push(new Player(user));
    });

    players.forEach((player) => {
      deck.deal(player.getHand(), 4)
      game.hands[player.name] = player.getHand().getCards();
    });

    game.discard = new Deck([deck.takeCard()]);

    game.deck = deck;

    this.Server.games[game.id] = game;
  }
}
