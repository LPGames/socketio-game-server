import {EnhancedWebSocket, ServerSocket} from "../socket";
import {RoomListeners} from "./room";

export interface WebSocketMessageData {
  action: string;
}

export interface WebSocketRoomMessageData extends WebSocketMessageData {
  room: string;
}

export interface WebSocketRoomUserMessageData extends WebSocketRoomMessageData {
  id: string;
}

export interface WebSocketRoomIndexMessageData extends WebSocketRoomMessageData {
  index: number;
}

export class ConnectionListeners {

  Socket: EnhancedWebSocket;
  Server: ServerSocket;
  RoomListeners: RoomListeners;

  constructor(socket: EnhancedWebSocket, serverSocket: ServerSocket) {
    this.Socket = socket;
    this.Server = serverSocket;
    this.RoomListeners = new RoomListeners(socket, serverSocket);
  }

  StartListeners = () => {
    const socket = this.Socket;
    const server = this.Server;
    const roomListeners = this.RoomListeners;

    server.RestartClientPing();
    this.Handshake(socket);

    socket.on("pong", function () {
      this.isAlive = true;

      if (server.debug) {
        console.log(`Socket pong [id=${this.id}]`);
      }
    });
    socket.on("close", function () {
      if (server.debug) {
        console.log(`Socket disconnected [id=${this.id}]`);
      }

      delete server.connections[this.id];

      // @todo We probably shouldnt notify EVERY connected user when someone disconnects.
      const activeUsers = Object.values(server.connections);
      server.SendMessage('user_disconnected', activeUsers, {user: this.id});

      const userRooms = Object.keys(server.roomConnections).filter((room) => Object.keys(server.roomConnections[room]).includes(this.id));
      userRooms.forEach((room) => {
        roomListeners.DisconnectRoom(room, this.id);
      });
      server.SendMessage('user_disconnected', activeUsers, {user: this.id});

      server.RestartClientPing();
    });
  }

  Handshake(socket: EnhancedWebSocket) {
    // check for reconnection attempt
    const reconnected = Object.keys(this.Server.users).includes(socket.uid);
    if (reconnected) {
      socket.id = this.Server.users[socket.uid];
      console.info(`This user has reconnected [uid=${socket.uid}] as [id=${socket.id}]`);
    }

    // Update the user and connection tables.
    this.Server.users[socket.uid] = socket.id;
    this.Server.connections[socket.id] = socket.id;

    if (reconnected) {
      const userRooms = Object.keys(this.Server.roomUsers).filter((room) => Object.keys(this.Server.roomUsers[room]).includes(socket.id));
      userRooms.forEach((room) => {
        this.RoomListeners.ReconnectRoom(room, socket.id);
      });
    }

    const users = Object.values(this.Server.users);
    console.info('Sending callback for new connection Handshake...');

    this.Server.SaveUsers();
    this.Server.SendMessage('user_handshake', [socket.id], {user: socket.id});
    this.Server.SendMessage('user_connected', users.filter((id) => id !== socket.id), {user: socket.id});
  }

}