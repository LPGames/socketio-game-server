import {EnhancedWebSocket, MessagePayloadData, ServerSocket} from "../socket";
import {v4} from "uuid";
import {WebSocketMessageData, WebSocketRoomUserMessageData} from "./connection";

export class RoomListeners {

  Socket: EnhancedWebSocket;
  Server: ServerSocket;

  constructor(socket: EnhancedWebSocket, serverSocket: ServerSocket) {
    this.Socket = socket;
    this.Server = serverSocket;
  }

  StartListeners = () => {
    const socket = this.Socket;
    const server = this.Server;
    const roomListeners = this;

    socket.on('message', function message(message: WebSocketMessageData, isBinary: boolean) {
      if (isBinary) {
        return;
      }

      const rawData = message.toString();
      const data = JSON.parse(rawData);
      console.log(isBinary);
      console.log(data);
      let roomData;

      switch (data.action) {
        case 'host':
          roomListeners.Host(this.id);
          break;
        case 'join-room':
          roomData = data as WebSocketRoomUserMessageData;
          roomListeners.JoinRoom(roomData.room, this.id);
          break;
        case 'leave-room':
          roomData = data as WebSocketRoomUserMessageData;
          roomListeners.LeaveRoom(roomData.room, this.id);
          break;
        default:
          break;
      }
    });


  }


  CreateRoom(room = ''): string {
    if (room.length == 0) {
      room = v4().substring(0, 5);
    }

    while (Object.values(this.Server.rooms).includes(room)) {
      room = v4().substring(0, 5);
    }

    console.log(`room ${room} was created`);
    this.Server.rooms[room] = room;
    this.Server.roomUsers[room] = {};
    this.Server.roomConnections[room] = {};

    return room;
  }

  DeleteRoom(room: string) {
    console.log(`room ${room} was deleted`);

    delete this.Server.rooms[room];
    delete this.Server.roomUsers[room];
    delete this.Server.roomConnections[room];

    this.Server.SaveRooms();
  }

  JoinRoom(room: string, id: string) {
    if (!Object.values(this.Server.rooms).includes(room)) {
      this.CreateRoom(room);
    }

    console.log(`room ${room} was joined with ${id}`);
    this.Server.roomUsers[room][id] = id;

    if (!Object.keys(this.Server.roomConnections).includes(room)) {
      this.Server.roomConnections[room] = {};
    }
    this.Server.roomConnections[room][id] = id;

    this.Server.SaveRooms();
    this.Server.SendMessage('user_joined', Object.values(this.Server.roomUsers[room]), {
      room,
      user: id,
      users: Object.values(this.Server.roomUsers[room])
    });
  }

  DisconnectRoom(room: string, socketId: string) {
    const roomConnections = Object.values(this.Server.roomConnections[room]);

    if (roomConnections.includes(socketId)) {
      delete this.Server.roomConnections[room][socketId];
    }
  }

  LeaveRoom(room: string, socketId: string) {
    console.log(`request to leave room: room ${room} and sid ${socketId}`);

    this.DisconnectRoom(room, socketId);
    const roomUsers = this.DoLeaveRoom(room, socketId);

    let payload: MessagePayloadData = {
      room: room,
      user: socketId,
    }

    roomUsers.length == 1 ? payload['users'] = [] : payload['users'] = Object.values(this.Server.roomUsers[room]);

    this.Server.SaveRooms();
    this.Server.SendMessage('user_left', roomUsers, payload);
  }

  DoLeaveRoom(room: string, socketId: string) {
    if (!this.IsValidRoom(room)) {
      console.error(`room ${room} does not exist`);
      return [socketId];
    }

    if (!this.IsValidRoomUser(room, socketId)) {
      console.error(`room ${room} does not exist`);
      return [socketId];
    }

    const roomUsers = Object.values(this.Server.roomUsers[room]);

    if (roomUsers.includes(socketId)) {
      delete this.Server.roomUsers[room][socketId];
    }

    if (Object.keys(this.Server.roomUsers[room]).length === 0) {
      this.DeleteRoom(room);
    }

    return roomUsers;
  }

  ReconnectRoom(room: string, socketId: string) {
    if (!Object.keys(this.Server.roomConnections).includes(room)) {
      this.Server.roomConnections[room] = {};
    }
    this.Server.roomConnections[room][socketId] = socketId;
  }

  Host(socketId: string) {
    const room = this.CreateRoom()
    this.JoinRoom(room, socketId)
  }

  IsValidRoom(room: string) {
    return Object.values(this.Server.rooms).includes(room);
  }

  IsValidRoomUser(room: string, socketId: string) {
    if (!Object.keys(this.Server.roomUsers).includes(room)) {
      return false;
    }

    return Object.values(this.Server.roomUsers[room]).includes(socketId);
  }

}
