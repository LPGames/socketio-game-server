import {ClientRequest, IncomingMessage, Server as HTTPServer} from 'http';
import {ConnectionListeners} from "./listeners/connection";
import {RoomListeners} from "./listeners/room";
import {GameListeners} from "./listeners/game";
import WebSocket, {WebSocketServer} from "ws";
import {v4} from "uuid";
import {Deck, ICard, IDeck} from "typedeck";
import * as fs from 'fs';

export interface EnhancedWebSocketServer extends WebSocketServer {
  on(event: "connection", cb: (this: EnhancedWebSocketServer, socket: EnhancedWebSocket, request: IncomingMessage) => void): this;

  on(event: "error", cb: (this: EnhancedWebSocketServer, error: Error) => void): this;

  on(event: "headers", cb: (this: EnhancedWebSocketServer, headers: string[], request: IncomingMessage) => void): this;

  on(event: "close" | "listening", cb: (this: EnhancedWebSocketServer) => void): this;

  on(event: string | symbol, listener: (this: EnhancedWebSocketServer, ...args: any[]) => void): this;

  clients: Set<EnhancedWebSocket>;
}

export interface MessagePayloadData {
  message?: string;
  user?: string;
  users?: string[];
  room?: string;
  game?: GameState;
  data?: string;
  responseType?: number;
}

export interface GameState {
  holding: ICard | null;
  id: string;
  currentUser: string;
  users: string[];
  deck: IDeck | null;
  discard: IDeck | null;
  hands: { [uid: string]: ICard[] };
  turn: number;
}

export interface EnhancedWebSocket extends WebSocket {
  on(event: "close", listener: (this: EnhancedWebSocket, code: number, reason: Buffer) => void): this;

  on(event: "error", listener: (this: EnhancedWebSocket, err: Error) => void): this;

  on(event: "upgrade", listener: (this: EnhancedWebSocket, request: IncomingMessage) => void): this;

  on(event: "message", listener: (this: EnhancedWebSocket, data: WebSocket.RawData, isBinary: boolean) => void): this;

  on(event: "open", listener: (this: EnhancedWebSocket) => void): this;

  on(event: "ping" | "pong", listener: (this: EnhancedWebSocket, data: Buffer) => void): this;

  on(
    event: "unexpected-response",
    listener: (this: EnhancedWebSocket, request: ClientRequest, response: IncomingMessage) => void,
  ): this;

  on(event: string | symbol, listener: (this: EnhancedWebSocket, ...args: any[]) => void): this;

  /**
   * The unique identifier for this socket connection.
   *
   * This is newly generated for each connection.
   */
  id: string;

  /**
   * The unique identifier for the user associated with this socket connection.
   *
   * This is passed in as a header when the connection is established.
   */
  uid: string;
  isAlive: boolean;
}

export class ServerSocket {
  public static instance: ServerSocket;

  public wss: EnhancedWebSocketServer;
  public pingInterval: number;
  public debug: boolean;

  public users: { [uid: string]: string };
  public connections: { [uid: string]: string };
  public rooms: { [rid: string]: string };
  public roomUsers: { [rid: string]: { [rid: string]: string } };
  public roomConnections: { [rid: string]: { [rid: string]: string } };
  public games: { [gid: string]: GameState };
  private pingRunner: ReturnType<typeof setInterval>;

  constructor(server: HTTPServer) {
    this.pingInterval = 10000;

    this.debug = false;

    ServerSocket.instance = this;
    this.users = {};
    this.connections = {};
    this.rooms = {};
    this.roomUsers = {};
    this.roomConnections = {};
    this.games = {};
    this.pingRunner = setInterval(() => {
    });

    this.ReloadAll();

    const socketServer =
      this.wss = new WebSocket.Server({
        server: server,
      }) as EnhancedWebSocketServer;

    this.StartServerListeners();
  }

  StartServerListeners = () => {
    this.wss.on('close', () => {
      if (this.debug) {
        console.info('`close` - The server has stopped listening for connections.');
      }
    });

    this.wss.on('connection', (websocket, request) => {
      if (this.debug) {
        console.info('`connection` - A new connection handshake has been received.');
      }
      let socket = websocket as EnhancedWebSocket
      socket.uid = request.headers.userid as string;
      this.StartSocketListeners(socket);
    });

    this.wss.on('error', (error: Error) => {
      if (this.debug) {
        console.error('`error` - An error has occurred in the webserver: ', error);
      }
    });

    this.wss.on('headers', (headers, request) => {
      if (this.debug) {
        console.info('`headers` - Headers have been received and are about to be sent as a response.');
      }

      // let socket = request.socket as EnhancedWebSocket;
      // socket.id = request.headers.userid as string;
    });

    this.wss.on('listening', () => {
      if (this.debug) {
        console.info('`listening` - The server has started listening for connections.');
      }
    });

    this.wss.on('wsClientError', () => {
      if (this.debug) {
        console.error('`wsClientError` - An error has occurred in the websocket client before the connection was established.');
      }
    });

    this.RestartClientPing();
  }

  RestartClientPing = () => {
    clearInterval(this.pingRunner);

    console.log(this.wss.clients.size);
    if (this.wss.clients.size === 0) {
      return;
    }

    // Set up a ping to keep the connection alive.
    this.pingRunner = setInterval(() => {

      if (this.debug) {
        console.log(`Pinging ${this.wss.clients.size} active clients...`);
      }
      this.wss.clients.forEach(function each(ws) {
        if (!ws.isAlive) {
          return ws.terminate();
        }
        ws.isAlive = false;
        ws.ping(() => {
        });
      });
    }, this.pingInterval);
  }

  StartSocketListeners = (socket: EnhancedWebSocket) => {
    socket.id = this.getUniqueID();
    socket.isAlive = true;

    new ConnectionListeners(socket, this).StartListeners();
    new RoomListeners(socket, this).StartListeners();
    new GameListeners(socket, this).StartListeners();
  }

  GetUidFromSocketId = (socketId: string) => Object.keys(this.users).find((uid) => this.users[uid] === socketId);

  SendMessage = (name: string, users: string[], payload: MessagePayloadData = {}) => {
    console.info('Emmitting event: ' + name + ' to users: ' + users);

    payload.message = name;
    payload.responseType = this.responseTypeMap[name] ?? '';

    this.wss.clients.forEach(function (ws: WebSocket) {
      const ews = ws as EnhancedWebSocket;
      if (users.includes(ews.id)) {
        const json = JSON.stringify(payload);
        console.info('Sending message: ' + json);
        ws.send(json);
      }
    });
  }

  SendRoomMessage = (name: string, room: string, payload: MessagePayloadData = {}) => {
    const users = this.GetRoomConnections(room);
    this.SendMessage(name, users, payload);
  }

  GetRoomConnections = (roomId: string): string[] => {
    return Object.keys(this.roomConnections[roomId] ?? {});
  }

  SaveUsers() {
    fs.writeFileSync('../users.json', JSON.stringify(this.users));
  }

  SaveGames() {
    fs.writeFileSync('../games.json', JSON.stringify(this.games));
  }

  SaveRooms() {
    fs.writeFileSync('../rooms.json', JSON.stringify(this.rooms));
    fs.writeFileSync('../room-users.json', JSON.stringify(this.roomUsers));
  }

  ReloadAll() {
    this.users = JSON.parse(fs.readFileSync('../users.json', 'utf8')) ?? {};
    this.games = {};
    const games = JSON.parse(fs.readFileSync('../games.json', 'utf8')) ?? {};
    for (const key in games) {
      const game = games[key];
      this.games[game.id] = this.ReloadGame(game);
    }

    this.rooms = JSON.parse(fs.readFileSync('../rooms.json', 'utf8')) ?? {};
    this.roomUsers = JSON.parse(fs.readFileSync('../room-users.json', 'utf8')) ?? {};
  }

  ReloadGame(game: any) : GameState {
    return {
      id: game.id,
      users: game.users,
      deck: new Deck(game.deck.cards),
      discard: new Deck(game.discard.cards),
      turn: game.turn,
      holding: game.holding,
      currentUser: game.currentUser,
      hands: game.hands,
    };
  }

  private getUniqueID() {
    return v4();
  }

  private responseTypeMap: { [name: string]: number } = {
    'user_handshake': 0,
    'user_connected': 1,
    'user_disconnected': 2,
    'user_joined': 3,
    'user_left': 4,
    'game_started': 10,
    'card_drawn': 11,
    'card_placed': 12,
    'turn_completed': 13,
    'player_knocked': 14,
    'interaction_error': 20,
  }

  ClearData() {
    fs.writeFileSync('../users.json', '{}');
    fs.writeFileSync('../games.json', '{}');
    fs.writeFileSync('../rooms.json', '{}');
    fs.writeFileSync('../room-users.json', '{}');
    this.ReloadAll();
  }
}
