import http from 'http';
import express from 'express';
import {ServerSocket} from "./socket";
import https from "https";
import * as fs from "fs";

const application = express();

/** Server Handling */

const cfg = {
  ssl: true,
  port: 1337,
  ssl_key: '../certs/private.key',
  ssl_cert: '../certs/certificate.crt'
};

let webServer;

if (cfg.ssl) {
  webServer = https.createServer({
    key: fs.readFileSync(cfg.ssl_key),
    cert: fs.readFileSync(cfg.ssl_cert)
  }, application);
  new ServerSocket(webServer);

} else {
  webServer = http.createServer(application);
  new ServerSocket(webServer);
}


/** Log the request */
application.use((req, res, next) => {
  console.info(`METHOD: [${req.method}] - URL: [${req.url}] - IP: [${req.socket.remoteAddress}]`);

  res.on('finish', () => {
    console.info(`METHOD: [${req.method}] - URL: [${req.url}] - STATUS: [${res.statusCode}] - IP: [${req.socket.remoteAddress}]`);
  });

  next();
});

/** Parse the body of the request */
application.use(express.urlencoded({extended: true}));
application.use(express.json());

/** Rules of our API */
application.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');

  if (req.method == 'OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
    return res.status(200).json({});
  }

  next();
});

/** Healthcheck */
application.get('/ping', (req, res, next) => {
  return res.status(200).json({hello: 'world!'});
});

/** Socket Information */
application.get('/status', (req, res, next) => {
  return res.status(200).json({
    users: ServerSocket.instance.users,
    connections: ServerSocket.instance.connections,
    rooms: ServerSocket.instance.rooms,
    roomUsers: ServerSocket.instance.roomUsers,
    roomConnections: ServerSocket.instance.roomConnections,
    games: ServerSocket.instance.games
  });
});

application.get('/reset', (req, res, next) => {
  ServerSocket.instance.ClearData();
  return res.status(200).json({
    status: 'ok',
  });
});


/** Error handling */
application.use((req, res, next) => {
  const error = new Error('Not found');

  res.status(404).json({
    message: error.message
  });
});

/** Listen */
webServer.listen(cfg.port, () => console.info(`Server is running`));