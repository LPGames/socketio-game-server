import React, {useContext} from 'react';
import './App.css';
import SocketContext from "./contexts/Socket/Context";


function clickMe() {

}

function App() {
  const {socket, uid, users, room, roomUsers} = useContext(SocketContext).SocketState;

  const HostRoom = () => {
    console.info('requesting new room to host');
    socket?.emit('host_room');
  }

  const JoinRoom = () => {
    const joinRoom = prompt('Enter room ID to join');
    console.info('requesting to join room '+ joinRoom);
    socket?.emit('join_room',  joinRoom);
  }

  const FindRoom = () => {
    console.info('requesting to find room');
    socket?.emit('find_room');
  }

  const LeaveRoom = () => {
    console.info('requesting to leave current room');
    socket?.emit('leave_room', room);
  }

  return (
    <div className="App">
      <header className="App-header">
        <h2>Socket IO: Information</h2>
        <p>
          Your user ID is: <code>{uid}</code><br/>
          Users online: <code>{users.length}</code><br/>
          Socket ID: <code>{socket?.id}</code><br/>

          {room &&
            <>
              Room ID: <code>{room}</code><br/>
              Users in room: <code>{roomUsers.length}</code><br/>
            </>
          }

        </p>

        {!room &&
          <>
            <div>
              <div onClick={HostRoom}>Host</div>
            </div>
            <div>
              <div onClick={JoinRoom}>Join</div>
            </div>
            <div>
              <div onClick={FindRoom}>Find</div>
            </div>
          </>
        }
        {room &&
          <div>
            <div onClick={LeaveRoom}>Leave</div>
          </div>
        }
      </header>


    </div>
  );
}

export default App;
