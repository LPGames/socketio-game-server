import React, {useEffect} from "react";
import {defaultSocketContextState, SocketContextProvider, SocketReducer} from "./Context";
import {useSocket} from "../../hooks/useSocket";

export interface ISocketContextComponentProps extends React.PropsWithChildren {
}

const SocketContextComponent: React.FunctionComponent<ISocketContextComponentProps> = (props) => {
  const {children} = props;

  const [SocketState, SocketDispatch] = React.useReducer(SocketReducer, defaultSocketContextState);
  const [loading, setLoading] = React.useState<boolean>(true);

  const socket = useSocket('ws://localhost:1337', {
    reconnectionAttempts: 5,
    reconnectionDelay: 1000,
    autoConnect: false,
  });

  useEffect(() => {
    socket.connect();

    SocketDispatch({type: 'update_socket', payload: socket});

    StartListeners();

    SendHandshake();
  }, []);

  const StartListeners = () => {
    socket.on('user_connected', (users: string[]) => {
      console.info('new user connected. user list received');
      SocketDispatch({type: 'update_users', payload: users});
    });

    socket.on('user_disconnected', (uid: string) => {
      console.info('other user disconnected');
      SocketDispatch({type: 'remove_user', payload: uid});
    });


    socket.io.on('reconnect', (attempt) => {
      console.info('reconnected on attempt: ' + attempt);
      SendHandshake();
    })
    socket.io.on('reconnect_attempt', (attempt) => {
      console.info('reconnection attempt: ' + attempt);
    })

    socket.io.on('reconnect_error', (error) => {
      console.error('reconnection error: ' + error);
    })

    socket.io.on('reconnect_failed', () => {
      console.error('reconnection failed');
      alert('reconnection failed');
    })

    /**
     * Custom
     */

    socket.on('spam', (message: string) => {
      console.info('Spam received: ' + message);
    });

    socket.on('user_clicked', (uid: string) => {
      console.info(`user ${uid} clicked`);
    });

    socket.on('user_stop', () => {
      console.info(`stop that!!!`);
    });

    socket.on('user_joined_room', (data: {room: string, uid: string, roomUsers: string[]}) => {
      console.log(data.room);
      console.log(data.uid);

      if (data.uid === socket.id) {
        console.info('you joined the room');
        SocketDispatch({type: 'update_room', payload: data.room});
      }
      else {
        console.info('another user joined the room');
      }

      SocketDispatch({type: 'update_room_users', payload: data.roomUsers});
    });

    socket.on('user_left_room', (data: {room: string, uid: string, roomUsers: string[]}) => {
      if (data.uid === socket.id) {
        console.info('you left the room');
        SocketDispatch({type: 'update_room', payload: ''});
        SocketDispatch({type: 'update_room_users', payload: []});
      }
      else {
        console.info('another user left the room');
        SocketDispatch({type: 'update_room_users', payload: data.roomUsers});
      }
    });
  };
  const SendHandshake = () => {
    console.info('sending Handshake now');

    socket.emit('handshake', (uid: string, users: string[]) => {
        console.info('Handshake callback message');
        SocketDispatch({type: 'update_uid', payload: uid});
        SocketDispatch({type: 'update_users', payload: users});

        setLoading(false);
      }
    );
  };

  if (loading) {
    return (<p>Loading</p>);
  }

  return (
    <div>
      <SocketContextProvider value={{SocketState, SocketDispatch}}>
        {children}
      </SocketContextProvider>
    </div>
  );
}

export default SocketContextComponent;