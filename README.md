# Game server proto using socket.io

### Resources

- [Youtube - Socket IO with React and Typescript](https://www.youtube.com/watch?v=-aTWWl4klYE)
- [Getting Started with Typescript and Socket.Io - Tutorial](https://tutorialedge.net/typescript/typescript-socket-io-tutorial/)
- [Create react app - Adding TypeScript](https://www.digitalocean.com/community/tutorials/setting-up-a-node-project-with-typescript)



### Running

I use nodemon as a dev dependency so you can start the project

- `cd server`
- `node_modules/.bin/nodemon`
- `http://localhost:1337`

- `cd client`
- `npm start`
- `http://localhost:3000`

### SSL Certificates

- Make a `certs` directory in the root of the project and place the following files within it.
- `openssl req -nodes -newkey rsa:2048 -keyout private.key -out request.csr`
- `openssl x509 -in request.csr -out certificate.crt -req -signkey private.key -days 365`
- `openssl rsa -in private.key -text > private.pem`
- `openssl x509 -inform PEM -in certificate.crt > public.pem`
